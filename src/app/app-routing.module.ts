import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ColoresComponent } from './colores/colores.component';


const routes: Routes = [
{ path: '',   redirectTo: '/colores', pathMatch: 'full' }, 
  { path: 'colores',  component: ColoresComponent }
  ]
 

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

