import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


const httpOptions = {
    headers: new HttpHeaders({
        'Access-Control-Request-Method': 'GET',
        'Access-Control-Request-Headers': 'authorization'
    })
};


@Injectable({
  providedIn: 'root'
})
export class ColorService {

	private url : any = "https://reqres.in/api/colors";
  constructor(
  	        protected httpClient: HttpClient
  	) { }

  getColores(){
  	return this.httpClient.get(this.url);
  }

}


