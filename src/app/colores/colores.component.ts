import { Component, OnInit } from '@angular/core';
import { ColorService } from 'src/app/services/color.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-colores',
  templateUrl: './colores.component.html',
  styleUrls: ['./colores.component.scss']
})
export class ColoresComponent implements OnInit {

	public data : any;
	public last : any;
	public paramspage : any;

  constructor(
  	private activeRouter: ActivatedRoute,
  	protected colorService : ColorService
  	) { }

  ngOnInit(): void {
  	this.activeRouter.queryParams.subscribe(params => {

	  	this.colorService.getColores().subscribe((data: any) => {
	  		this.paramspage = 0;
	  		if (params.page)
	  			this.paramspage = params.page
	  		console.log(this.paramspage);

	  		this.data = data.data;
	  		
	  		if(this.data.length <= 3*this.paramspage+3){
	  			this.last = true;
	  		}
	  		else
	  			this.last = false;

	  		this.data = this.data.slice(3*this.paramspage, 3*this.paramspage+3);
	  		console.log(this.data);

	  	}, (error: any) => {
	  		console.error(error);
	  	}, () => {});

  	});

  }

copiar(element) {

  var aux = document.createElement("input");
  aux.setAttribute("value", document.getElementById(element).innerHTML);
  document.body.appendChild(aux);
  aux.select();
  document.execCommand("copy");
  console.log("Color "+aux.value+" copiado")
  document.body.removeChild(aux);
}


}
