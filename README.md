# Colorize

Generado con [Angular CLI](https://github.com/angular/angular-cli) version 9.1.6.

## Servidor de Desarrollo

Ejecute `ng serve` para un servidor de desarrollo. Navegue a `http: // localhost: 4200 /`. La aplicación se volverá a cargar automáticamente si cambia alguno de los archivos de origen.

## Compilacion

Ejecute `ng build` para compilar el proyecto. Los artefactos de compilación se almacenarán en el directorio `dist /`. Use el indicador `--prod` para una compilación de producción.

## Test unitarios

Ejecute `ng test` para ejecutar los test unitarias a través de [Karma](https://karma-runner.github.io).

## Text e2e

Ejecute `ng e2e` para ejecutar las pruebas de extremo a extremo a través de [Protractor](http://www.protractortest.org/).

